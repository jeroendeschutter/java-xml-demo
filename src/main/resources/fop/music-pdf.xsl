<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:fo="http://www.w3.org/1999/XSL/Format"
        xmlns:m="http://www.realdolmen.com/education/music">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4" page-height="29.7cm" page-width="21.0cm" margin="2cm">
                    <fo:region-body/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="A4">
                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates select="//m:track"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="m:track">
        <fo:block font-size="20pt" font-weight="bold" margin-bottom="0.25cm" color="green">
            <xsl:value-of select="m:title/text()"/>
        </fo:block>
         <fo:table margin-bottom="0.5cm">
             <fo:table-column column-width="2.5cm"/>
             <fo:table-column column-width="10cm"/>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell><fo:block font-weight="bold">Title:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block><xsl:value-of select="m:genre"/></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block font-weight="bold">Performer:</fo:block></fo:table-cell>
                    <fo:table-cell>
                        <fo:block>
                            <xsl:value-of select="m:performer/@name"/>
                            <fo:table margin-left="0.25cm">
                                <fo:table-body>
                                    <xsl:apply-templates select="m:performer/m:member"/>
                                </fo:table-body>
                            </fo:table>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block font-weight="bold">Genre:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block><xsl:value-of select="m:genre"/></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block font-weight="bold">Length:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block><xsl:value-of select="m:length"/></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell><fo:block font-weight="bold">Year:</fo:block></fo:table-cell>
                    <fo:table-cell><fo:block><xsl:value-of select="m:year"/></fo:block></fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
    </xsl:template>

    <xsl:template match="m:member">
        <fo:table-row>
            <fo:table-cell>
                <fo:block font-size="10pt" font-style="italic">
                    <xsl:value-of select="@firstName"/>&#160;<xsl:value-of select="@lastName"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

</xsl:stylesheet>
