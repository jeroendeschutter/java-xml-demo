package com.realdolmen.java.xml.stax;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import static javax.xml.stream.XMLStreamConstants.START_ELEMENT;

public class StaxMovieStreamReader {
    public static void main(String[] args) throws URISyntaxException, FileNotFoundException, XMLStreamException {
        Map<String, Integer> genres = new HashMap<>();

        FileInputStream fis = new FileInputStream(new File(ClassLoader.getSystemResource("movies.xml").toURI()));
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = factory.createXMLStreamReader(fis);
        while (reader.hasNext()) {
            int eventType = reader.next();
            if (eventType == START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("type")) {
                    String genre = reader.getElementText().trim().toLowerCase();
                    if (genres.containsKey(genre)) {
                        genres.put(genre, genres.get(genre) + 1);
                    } else {
                        genres.put(genre, 1);
                    }
                }
            }
        }
        System.out.println("\n*** GENRES ***\n");
        genres.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue().reversed()).forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
    }
}
