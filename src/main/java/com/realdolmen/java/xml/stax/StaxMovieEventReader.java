package com.realdolmen.java.xml.stax;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class StaxMovieEventReader {
    public static void main(String[] args) throws URISyntaxException, FileNotFoundException, XMLStreamException {
        Map<String, Integer> genres = new HashMap<>();

        FileInputStream fis = new FileInputStream(new File(ClassLoader.getSystemResource("movies.xml").toURI()));
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader reader = factory.createXMLEventReader(fis);
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {
                if (event.asStartElement().getName().getLocalPart().equalsIgnoreCase("type")) {
                    String genre = reader.getElementText().trim().toLowerCase();
                    if (genres.containsKey(genre)) {
                        genres.put(genre, genres.get(genre) + 1);
                    } else {
                        genres.put(genre, 1);
                    }
                }
            }
        }
        System.out.println("\n*** GENRES ***\n");
        genres.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue().reversed()).forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
    }
}
