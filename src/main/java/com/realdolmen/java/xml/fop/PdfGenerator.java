package com.realdolmen.java.xml.fop;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class PdfGenerator {
    private final TransformerFactory transformerFactory;
    private final FopFactory fopFactory;

    public static void main(String[] args) throws Exception {
        new PdfGenerator().generate(
                classPathResource("/fop/music.xml"),
                classPathResource("/fop/music-pdf.xsl"),
                new File("music.pdf")
        );
    }

    private static File classPathResource(String classpathResource) {
        return new File(PdfGenerator.class.getResource(classpathResource).getFile());
    }

    public PdfGenerator() throws Exception {
        transformerFactory = TransformerFactory.newInstance();
        fopFactory = FopFactory.newInstance(classPathResource("/fop/fop-config.xml"));
    }

    private void generate(File xml, File xsl, File pdf) throws Exception {
        final Source xmlSource = new StreamSource(xml);
        final Source xslSource = new StreamSource(xsl);

        try(BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(pdf))) {
            renderPdf(output, renderFo(xmlSource, xslSource));
        }
    }

    private void renderPdf(BufferedOutputStream output, DOMResult foResult) throws FOPException, TransformerException {
        Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, output);
        Transformer foToPdfTransformer = transformerFactory.newTransformer();
        foToPdfTransformer.transform(new DOMSource(foResult.getNode()), new SAXResult(fop.getDefaultHandler()));
    }

    private DOMResult renderFo(Source xmlSource, Source xslSource) throws TransformerException {
        Transformer xmlToFoTransformer = transformerFactory.newTransformer(xslSource);
        DOMResult foResult = new DOMResult();
        xmlToFoTransformer.transform(xmlSource, foResult);
        return foResult;
    }
}
