package com.realdolmen.java.xml.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class MovieParser extends DefaultHandler {

    boolean insideMovie = false;
    boolean insideType = false;
    Map<String, Integer> genres = new HashMap<>();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(localName.equals("title")) { insideMovie = true; }
        if(localName.equals("type")) { insideType = true; }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(localName.equals("title")) { insideMovie = false; }
        if(localName.equals("type")) { insideType = false; }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String chars = new String(ch, start, length).trim();
        if(insideMovie) {
            System.out.println(chars);
        } else if (insideType) {
            String genre = chars.toLowerCase();
            if (genres.containsKey(genre)) {
                genres.put(genre, genres.get(genre) + 1);
            } else {
                genres.put(genre, 1);
            }
        }
    }

    public void listGenres() {
        System.out.println("\n*** GENRES ***\n");
        genres.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue().reversed()).forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
    }

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, URISyntaxException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        SAXParser saxParser = factory.newSAXParser();
        MovieParser myParser = new MovieParser();

        saxParser.parse(new File(ClassLoader.getSystemResource("movies.xml").toURI()), myParser);
        myParser.listGenres();
    }
}
