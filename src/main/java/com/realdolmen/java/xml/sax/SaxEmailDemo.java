package com.realdolmen.java.xml.sax;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;

public class SaxEmailDemo extends DefaultHandler {

    String to = "";
    boolean foundTo = false, foundName = false;
    public void startElement(String namespaceURI, String name, String qname, Attributes attr) {
        if (name.equals("to"))
            foundTo = true;
        else if (foundTo && name.equals("name"))
            foundName = true;
    }
    public void characters(char[] ch, int start, int length) {
        if (foundName)
            to += new String(ch, start, length);
    }
    public void endElement(String namespaceURI, String
            name, String qname){
        if (foundName && name.equals("name")){
            foundTo = false; foundName = false;
            System.out.println("To: " + to);
            to = "";
        }
    }

    public static void main(String[] args) throws URISyntaxException, FileNotFoundException {
        try {
            SAXParserFactory sf= SAXParserFactory.newInstance();
            sf.setNamespaceAware(true);
            SAXParser parser = sf.newSAXParser();
            parser.parse(new File(ClassLoader.getSystemResource("email.xml").toURI()), new SaxEmailDemo());
        }catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
    }
}
