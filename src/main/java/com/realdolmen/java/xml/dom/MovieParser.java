package com.realdolmen.java.xml.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class MovieParser {

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, URISyntaxException {
        Map<String, Integer> genres = new HashMap<>();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document d = db.parse(ClassLoader.getSystemResource("movies.xml").toString());
        NodeList trackNodes = d.getElementsByTagName("type");
        for(int i = 0; i < trackNodes.getLength(); i++) {
            Element genre = (Element)trackNodes.item(i);
            String genreName = genre.getTextContent().trim().toLowerCase();
            if (genres.containsKey(genreName)) {
                genres.put(genreName, genres.get(genreName) + 1);
            } else {
                genres.put(genreName, 1);
            }
        }
        System.out.println("\n*** GENRES ***\n");
        genres.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed()).forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
    }


}
