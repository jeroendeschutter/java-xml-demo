package com.realdolmen.java.xml.jaxb;

import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType
public class Movie {

	private String title;
	private String type;
	private String format;

	public String getTitle() {
		return title;
	}
	@XmlJavaTypeAdapter(value=MyAdapter.class)
	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}
	@XmlJavaTypeAdapter(value=MyAdapter.class)
	public void setType(String type) {
		this.type = type;
	}

	public String getFormat() {
		return format;
	}
	@XmlJavaTypeAdapter(value=MyAdapter.class)
	public void setFormat(String format) {
		this.format = format;
	}
	
	@Override
	public String toString() {
		return "Movie [title=" + title + ", type=" + type + ", format=" + format + "]";
	}
	
	

}
