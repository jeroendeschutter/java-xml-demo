package com.realdolmen.java.xml.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Reads a Movies object from an XML file
 * Adds a new movie
 * Orders the movies by title
 * Writes the modified Movies object to a new XML file
 */
public class MovieMarshaller {
    public static void main(String[] args) throws JAXBException {
        JAXBContext movieContext = JAXBContext.newInstance(Movies.class);
        Unmarshaller unmarshaller = movieContext.createUnmarshaller();
        Movies myMovies = (Movies)unmarshaller.unmarshal(ClassLoader.getSystemResource("movies.xml"));

        Movie movie = new Movie();
        movie.setTitle("Star Wars Episode IV : A New Hope");
        movie.setFormat("Blu Ray");
        movie.setType("sci-fi");
        myMovies.getMovies().add(movie);

        List<Movie> sortedMovies = new ArrayList<Movie>(myMovies.getMovies());
        Collections.sort(sortedMovies, new Comparator<Movie>() {
            public int compare(Movie o1, Movie o2) {
                return o1.getTitle().compareTo(o2.getTitle());
            }
        });
        myMovies.setMovies(sortedMovies);

        Marshaller marshaller = movieContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(myMovies, new File("JaxbSortedMovies.xml"));
    }
}
