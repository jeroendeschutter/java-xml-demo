package com.realdolmen.java.xml.jdom;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
* Reads a list of movies from an XML file.
 * Adds ratin attribute to each movie.
 * Adds a new movie to the list.
 * Orders the list of movies by title.
 * Writes the modified list to a new XML file.
 */
public class MovieParser {

    public static void main(String[] args) throws FileNotFoundException, JDOMException, IOException, URISyntaxException {
        SAXBuilder builder = new SAXBuilder();
        Document input = builder.build(ClassLoader.getSystemResource("movies.xml"));

        List<Element> movies = input.getRootElement().getChildren("movie");
        for (Element movie : movies) {
            String genre = movie.getChildTextTrim("type");
            if (genre.equalsIgnoreCase("romance")) {
                movie.setAttribute("rating", ":-(");
            } else {
                movie.setAttribute("rating", "NOT_RATED");
            }
        }

        Element tesb = new Element("movie");
        tesb.setAttribute("rating", "10/10")
                .addContent(new Element("title").addContent("Star Wars Episode V : The Empire Strikes Back"))
                .addContent(new Element("type").addContent("Space Opera"))
                .addContent(new Element("format").addContent("on DVD"));
        input.getRootElement().addContent(tesb);

        List<Element> sortedMovies = new ArrayList<>(input.getRootElement().getChildren("movie"));
        sortedMovies
                .sort((Element e1, Element e2) -> e1.getChild("title").getTextTrim().compareToIgnoreCase(e2.getChild("title").getTextTrim()));
        input.getRootElement().setContent(sortedMovies);

        Format format = Format.getPrettyFormat( );
        XMLOutputter outputter = new XMLOutputter(format);
        FileOutputStream outputStream = new FileOutputStream("JDomSortedMovies.xml");
        outputter.output(input, outputStream);

    }


}
